void wipeEeprom(int startByte = 0, int endByte = 512)
{
  Serial.println("[INFO] Wiping EEPROM");
  for (int address = startByte; address < endByte; ++address)
  {
    EEPROM.update(address, 0);
  }
  delay(100);
  EEPROM.commit();
  Serial.println("[INFO] EEPROM Wiped Successfully");
}