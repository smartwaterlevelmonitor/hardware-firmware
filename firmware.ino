#include <EEPROM.h>             //Low Level Library for accessing EEPROM on ESP8266
#include <ESP8266WiFi.h>        //Library to handle WiFi and AccessPoint using ESP8266
#include <BlynkSimpleEsp8266.h> //Set of APIs to use Blynk Cloud

//---------------Constants declaration--------------------------------------------------------------
const int DEPTH_SENSOR_TRANSMITTER_PIN = 5; // D1 on NodeMCU board
const int DEPTH_SENSOR_RECEIVER_PIN = 4;    // D2 on NodeMCU board
const String DEVICE_NAME = "IoT Smart Meter";
const String WIFI_AP_NAME = DEVICE_NAME;

//---------------Variables declaration--------------------------------------------------------------
double currentDepth;
int depthTimerID;
String blynkAuthToken = "3Ke-8peEPpFF43SbPQCN28cxACAhsa8t";
String wifiSsid = "papa ko bolo net karaye";
String wifiPassword = "Hello@786";
String tankDepth = "1000";

//-------------Functions declaration---------------------------------------------------------------
void initializeComponents(void);
void setupPins(void);
void setFunctionCallbacks(void);
void setupDevice(void);
double getWaterLevel(int DEPTH_SENSOR_TRANSMITTER_PIN, int DEPTH_SENSOR_RECEIVER_PIN);
void updateWaterLevelWidget(void);
void wipeEeprom(int startByte, int endByte);

//-----------Objects declaration-------------------------------------------------------------------
BlynkTimer Timer;
WidgetTerminal terminal(V3);

void setup()
{
  initializeComponents();
}

void loop()
{
  if (!Blynk.connected())
  {
    setupDevice();
  }
  Blynk.run();
  Timer.run();
}