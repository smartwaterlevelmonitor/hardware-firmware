void setupPins(void)
{
  pinMode(DEPTH_SENSOR_TRANSMITTER_PIN, OUTPUT);
  pinMode(DEPTH_SENSOR_RECEIVER_PIN, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void initializeComponents(void)
{
  Serial.begin(115200);
  setupPins();
  setFunctionCallbacks();
  EEPROM.begin(1024);
  Blynk.connectWiFi(wifiSsid.c_str(), wifiPassword.c_str());
  Blynk.begin(blynkAuthToken.c_str(), wifiSsid.c_str(), wifiPassword.c_str());
}

void setFunctionCallbacks(void)
{
  depthTimerID = Timer.setInterval(5000L, updateWaterLevelWidget);
}